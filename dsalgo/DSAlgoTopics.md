# Data Structures & Algorithms Topics

### 0 Big O
- [ ] Asymptotic Analysis

### 1 Arrays
- [x] Insertion
- [x] Deletion
- Searching
    - [x] Linear Search
    - [x] Binary Search
- [ ] Duplicate Check
- [ ] Random Shuffle

### 2 Simple Sorting
- [x] Bubble Sort
- [x] Selection Sort
- [x] Insertion Sort
- [ ] Sorting Objects

### 3 Stack
- [x] Stack implementation using Array
- [x] Reversing a String using Stack
- [x] Bracket Checker using Stack
- [ ] Infix to Postfix
    
### 4 Queues
- [x] Queue implementation using Array
- [x] Circular Queue
- [x] Priority Queue using Array

### 5 Linked Lists
- [x] Implement insert, delete, find using -
    -  [x] Singly Linked List 
    -  [x] Double-ended or Circular Linked List
    -  [x] Doubly Linked List
- [x] Implement Abstract Data-Type(ADT) using a Linked List 
    -  [x] Implement Stack using a Linked List
    -  [x] Implement Queue using a Linked List
- [x] Sorted Linked List
- [ ] Remove duplicates from Linked List
- [ ] Concatenate 2 Linked Lists
- [ ] Merge 2 Linked Lists
- [ ] Reverse a Linked List
    
### 6 Recursion
- [x] Factorials
- [x] Fibonacci
- [x] Binary Search
- [ ] Towers of Hanoi
- [ ] Anagrams

### 7 Advanced Sorting
- [x] Merge Sort 
     -  [x] Recursive
     -  [x] Iterative
- [x] Shell Sort using Comparable
- [x] Quick/Partition Sort using Comparator
    
### 8 Binary Search Trees
- [x] Implement Binary Search Tree using Linked List
    -  [x] Insertion
    -  [x] Search
    -  [x] Deletion
    -  Display Recursive Traversals
          - [x] Preorder
          - [x] Inorder
          - [x] Postorder
    -  Display Iterative Traversals
          - [x] Preorder
          - [x] Inorder
          - [x] Postorder
    -  Display Traversals without Stack or Queue (Threaded Binary Tree)
          - [ ] Preorder
          - [ ] Inorder
          - [ ] Postorder
    - [x] Level Order Traversal
    - [ ] Huffman Coding for keys compression

### 9 Balanced Search Trees
- [x] *Implement AVL Tree (Not mandatory)* 
    - [x] Insertion
    - [x] Search
    - [x] Deletion
    - [x] Display
- [ ] *Implement Red Black Tree (Not mandatory)* 
    - [x] Insertion
    - [x] Search
    - [ ] Deletion
    - [x] Display
- [ ] *Implement 2-3 Tree (Not mandatory)* 
    - [ ] Insertion
    - [ ] Search
    - [ ] Deletion
    - [ ] Display
- [ ] *Implement 2-3-4 Tree (Not mandatory)* 
    - [ ] Insertion
    - [ ] Search
    - [ ] Deletion
    - [ ] Display
- [ ] *Implement Interval Search Tree (Not mandatory)* 
    - [ ] Insertion
    - [ ] Search
    - [ ] Deletion
    - [ ] Display
             
### 10 Heaps
- [x] Implement using Array
    -  Insertion
    -  Search
    -  Deletion
    -  Display
- [x] Heap Sort
- [x] Implement Priority Queue using Heap
- [x] Create a Heap using Heapify 

### 11 Hash Tables
- [x] Implement using Array - Linear Probing
    -  Insertion
    -  Search
    -  Deletion
    -  Display
- [x] Implement using Array - Quadratic Probing
    -  Insertion
    -  Search
    -  Deletion
    -  Display
- [x] Implement using Array - Double Hashing
    -  Insertion
    -  Search
    -  Deletion
    -  Display
- [x] Implement using Linked List - Double Hashing
    -  Insertion
    -  Search
    -  Deletion
    -  Display

### 12 Graphs
- [x] Graph Representation
    - [x] Adjacency Matrix
    - [x] Adjacency List
- [x] Undirected Graph    
    - [x] Implement Breadth First Search
    - [x] Implement Depth First Search
    - [x] Shortest path from vertex u to v
    - [x] Connected components 
    - [x] Cycle Detection 
    - [x] Check if a Graph is bipartite
    - [x] Find all bridges in a graph
    - [ ] Find all articulation points in a graph
- [x] Directed Graph 
    - [x] Topological Sort
    - [x] Cycle Detection
    - [x] Depth-First Order
    - [x] Strongly connected components
        - [x] Kosaraju-Sharir's Algorithm

### 13 Advanced Graph Algorithms
- [x] Edge-Weighted Digraph
    - [x] Representation                           
- [x] Minimum Cost Spanning Tree
    - [x] Prim's Algorithm
    - [x] Kruskal's Algorithm
- [ ] Shortest Path Algorithm
    - [x] Dijkstra's (single-source, supports positive edges only)
    - [ ] Bellman Ford (single-source, supports negative edges also)
    - [ ] Floyd Warshall (all pair)
- [ ] Max Flow
    - [ ] Ford-Fulkerson
    
### 14 Tries
- [x] Representation
    - [x] Using Arrays
    - [x] Using HashMap
- [x] Operations
    - [x] Insertion
    - [x] Deletion
    - [x] String Search     
    - [x] Prefix Search

### 15 Substring Search
- [x] Brute-Force
- [x] Knutt-Morris-Pratt (KMP)
- [ ] Boyer-Moore
- [x] Rabin-Karp

### 16 Union-Find / Disjoint Sets
- [x] Implement Quick-Find
- [x] Implement Quick-Union

### 17 Dynamic Programming
- [ ] Memoization
- [ ] Tabulation
- [ ] Problems
    https://www.youtube.com/playlist?list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr

### 18 Backtracking
- [ ] Problems
    - [ ] N Queens
    - [ ] Sum of Subsets
    - [ ] Graph Coloring
    - [ ] Hamiltonian Cycle

### 19 Branch & Bound
- [ ] Problems
    - [ ] Job Sequencing
    - [ ] 0/1 Knapsack
    - [ ] Travelling Salesman 

### 20 Bit Manipulation Techniques
- [ ] Bitwise Operators 
    - [ ] Complement (~)
    - [ ] And (&)
    - [ ] Or (|)
    - [ ] XOR (^)
    - [ ] Left Shift (<<)
    - [ ] Right Shift (>>)

### 21 Matrices
- [ ] Diagonal Matrix
- [ ] Lower Triangular Matrix
- [ ] Upper Triangular Matrix
- [ ] Tri-Diagonal Matrix
- [ ] Toeplitz Matrix
- [ ] Sparse Matrix

###22 Miscellaneous 
- [ ] Segment Trees
- [ ] Suffix Tree
- [ ] Suffix Array